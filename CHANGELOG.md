Change-Log
===============
> Regular configuration update: _01.09.2019_

More **detailed changelog** for each respective version may be viewed by pressing on a desired _version's name_.

## Version 1.x ##

### [0.0.1](https://bitbucket.org/gradle-universum/android-deployment/wiki/version/0.x) ###
> upcoming